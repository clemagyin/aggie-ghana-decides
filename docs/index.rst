.. Aggie documentation master file, created by
   sphinx-quickstart on Thu Jun 23 16:58:49 2016.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Aggie's Documentation!
=================================

.. toctree::
   :maxdepth: 2



   content/intro/introduction
   content/installation
   content/settings/settings
   content/smtc/smtc
   content/usingaggie/usingaggie
   content/usermanagement/usermanagement
  




Indices and Tables
==================

* :ref:`genindex`
* :ref:`search`
